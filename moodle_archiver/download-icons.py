#!/usr/bin/env python3

# Copyright (C) 2019  Michael Schertenleib
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
    download-icons

    This downloads icons from the moodle repository and saves them in icons.zip.
"""

import json
import re
import shutil
import urllib.request
import zipfile
from typing import Any, Optional
from pathlib import Path


def get(url: str) -> str:
    file_ = urllib.request.urlopen(url)
    content = file_.read().decode("utf-8")
    file_.close()

    return content


def download_to_archive(
    archive: Any, url: str, name: Optional[str] = None
) -> None:
    print("downloading " + url)

    if not name:
        name = Path(url).name

    with urllib.request.urlopen(url) as src, archive.open(name, "w") as dst:
        shutil.copyfileobj(src, dst)


def download_icons() -> None:
    repo_base_url = "https://raw.githubusercontent.com/moodle/moodle/master"
    file_types_url = repo_base_url + "/lib/classes/filetypes.php"
    modules_url = repo_base_url + "/lib/classes/plugin_manager.php"

    file_types = {}

    for match in re.finditer(
        "'type' => '([^']+)'.+?'icon' => '([^']+)'",
        get(file_types_url),
        re.DOTALL,
    ):
        type_, icon = match.groups()
        file_types[type_] = icon

    type_names = set(file_types.values())

    module_names = re.sub(
        "[\\s']",
        "",
        re.findall("(?<='mod' => array\\()[^)]+", get(modules_url))[0],
    ).split(",")

    with zipfile.ZipFile("icons.zip", "w") as archive:
        for type_name in type_names:
            icon_url = "{}/pix/f/{}-64.png".format(repo_base_url, type_name)
            icon_name = "type/{}.png".format(type_name)

            download_to_archive(archive, icon_url, icon_name)

        for module_name in module_names:
            icon_url = "{}/mod/{}/pix/icon.svg".format(
                repo_base_url, module_name
            )
            icon_name = "mod/{}.svg".format(module_name)

            download_to_archive(archive, icon_url, icon_name)

        download_to_archive(
            archive, repo_base_url + "/pix/f/FileTypesIcons-LICENSE.txt"
        )
        download_to_archive(
            archive, repo_base_url + "/pix/f/Oxygen-LICENSE.txt"
        )

        with archive.open("types.json", "w") as file_:
            file_.write(json.dumps(file_types, indent=4).encode("utf-8"))

        with archive.open("modules.json", "w") as file_:
            file_.write(json.dumps(module_names, indent=4).encode("utf-8"))


if __name__ == "__main__":
    download_icons()

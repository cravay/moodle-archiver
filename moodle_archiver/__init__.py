#!/usr/bin/env python3

# Copyright (C) 2019  Michael Schertenleib
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
    moodle-archiver

    This tool can download courses from Moodle using its REST API.
"""

import argparse
import datetime
import glob
import json
import logging
import re
import shutil
import urllib.error
import urllib.parse
import urllib.request
import uuid
import zipfile
from pathlib import Path
from typing import Any, Dict, Iterator, List, Optional, Set, Type

import pkg_resources

logger = logging.getLogger("moodle_archiver")


class Course:
    def __init__(self, id_: int, name: str, sections: "List[Section]") -> None:
        self.id = id_
        self.name = name
        self.sections = sections

    @staticmethod
    def from_dict(id_: int, name: str, dict_: dict) -> "Course":
        sections = [Section.from_dict(_) for _ in dict_]

        return Course(id_, name, sections)

    def download_files(self, downloader: "Downloader") -> None:
        for section in self.sections:
            section.download_files(downloader)

    def to_html(self, base_url: str = "") -> str:
        original_url = "{}/course/view.php?id={}".format(base_url, self.id)
        today = datetime.date.today()
        sections_html = "<hr>".join([_.to_html() for _ in self.sections])

        return """<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="generator" content="moodle-archiver">
<meta name="original-url" content="{}">
<meta name="download-date" content="{}">
<title>{}</title>
<style>
body {{ font-family: "Helvetica Neue", Helvetica, Arial, sans-serif; }}
.container {{ max-width: 1000px; margin: auto; padding: 20px; }}
.module, .folder-name, .file {{ min-height: 36px; }}
.module > a, .file > a {{ color: black; }}
.icon {{ padding-right: 5px; vertical-align: text-bottom;
                width: 24px; height: 24px; }}
.description {{ margin: 18px 0; }}
.folder {{ list-style: none; }}
a {{ text-decoration: none; color: blue; }}
h1 {{ font-size: 32px; margin: 50px 0 30px 0; }}
h2 {{ font-size: 24px; margin: 18px 0 12px 0; }}
table {{ border-collapse: collapse; }}
hr {{ background-color: #AAA; height: 1px; margin: 20px 0; border: none; }}
</style>
</head>
<body>
<div class="container">
<h1>{}</h1>
<div class="sections">{}</div>
</div>
</body>
</html>
""".format(
            original_url, today, self.name, self.name, sections_html
        )


class Section:
    def __init__(
        self, id_: int, name: str, summary: str, modules: "List[Module]"
    ) -> None:
        self.id = id_
        self.name = name
        self.summary = summary
        self.modules = modules

    @staticmethod
    def from_dict(dict_: dict) -> "Section":
        id_ = dict_["id"]
        name = dict_["name"]
        summary = dict_["summary"]
        modules = [Module.from_dict(_) for _ in dict_["modules"]]

        return Section(id_, name, summary, modules)

    def download_files(self, downloader: "Downloader") -> None:
        if self.summary:
            self.summary = downloader.download_linked_files(self.summary)

        for module in self.modules:
            module.download_files(downloader)

    def to_html(self) -> str:
        summary_html = ""
        if self.summary:
            summary_html = '<div class="summary">{}</div>'.format(self.summary)

        modules_html = ""
        if self.modules:
            modules_html = "".join([_.to_html() for _ in self.modules])

        return '<div class="section"><h2>{}</h2>{}{}</div>'.format(
            self.name, summary_html, modules_html,
        )


def icon_tag(url: str) -> str:
    return '<img src="{}" alt="icon" class="icon">'.format(url)


class Module:
    show_icon = True
    use_url = False

    def __init__(
        self,
        id_: int,
        type_: str,
        name: str,
        description: str,
        icon_url: str = "",
        url: str = "",
    ) -> None:
        self.id = id_
        self.type = type_
        self.name = name
        self.description = description
        self.icon_url = icon_url
        self.url = url

    @classmethod
    def get_kwargs_from_dict(cls, dict_: dict) -> dict:
        return {
            "id_": dict_["id"],
            "type_": dict_["modname"],
            "name": dict_["name"],
            "description": dict_.get("description", None),
            "icon_url": dict_["modicon"],
        }

    @staticmethod
    def from_dict(dict_: dict) -> "Module":
        module_classes: Dict[str, Type["Module"]] = {
            "label": LabelModule,
            "resource": ResourceModule,
            "folder": FolderModule,
            "url": URLModule,
            "page": PageModule,
        }

        module_class = module_classes.get(dict_["modname"], Module)

        kwargs = module_class.get_kwargs_from_dict(dict_)
        module = module_class(**kwargs)

        return module

    def download_files(self, downloader: "Downloader") -> None:
        if self.description:
            self.description = downloader.download_linked_files(
                self.description
            )

        if self.show_icon and self.icon_url:
            self.icon_url = downloader.copy_module_icon(
                self.type, self.icon_url
            )

    def to_html(self) -> str:
        name_html = ""

        if self.show_icon and self.icon_url:
            name_html = icon_tag(self.icon_url) + self.name

        if self.use_url and self.url:
            name_html = '<a href="{}">{}</a>'.format(self.url, name_html)

        description_html = ""

        if self.description:
            description_html = '<div class="description">{}</div>'.format(
                self.description
            )

        return '<div class="module">{}{}</div>'.format(
            name_html, description_html
        )


class LabelModule(Module):
    show_icon = False


class URLModule(Module):
    use_url = True

    @classmethod
    def get_kwargs_from_dict(cls, dict_: dict) -> dict:
        kwargs = super().get_kwargs_from_dict(dict_)

        if "contents" not in dict_:
            logger.warning("link-module %s doesn't have an url", kwargs["id_"])
        else:
            if len(dict_["contents"]) > 1:
                logger.warning(
                    "ignoring additional urls in link-module %s", kwargs["id_"]
                )

            kwargs["url"] = dict_["contents"][0]["fileurl"]

        return kwargs


class DownloadableModule(Module):
    class Folder:
        def __init__(self) -> None:
            self.folders: Dict[str, "FolderModule.Folder"] = {}
            self.files: List["FolderModule.File"] = []

        def download_files(self, downloader: "Downloader", path: Path) -> None:
            for name, folder in self.folders.items():
                folder.download_files(downloader, path / name)

            for file_ in self.files:
                file_.download(downloader, path)

        def to_html(self, folder_icon: str) -> str:
            html = '<ul class="folder">'

            for name, folder in self.folders.items():
                html += '<li><div class="folder-name">{}{}</div>{}</li>'.format(
                    icon_tag(folder_icon), name, folder.to_html(folder_icon)
                )

            for file_ in self.files:
                html += '<li class="file">{}</li>'.format(file_.to_html())

            html += "</ul>"

            return html

    class File:
        def __init__(
            self, name: str, url: str, file_type: str, sort_order: int
        ) -> None:
            self.name = clean_file_name(name)
            self.url = url
            self.file_type = file_type
            self.sort_order = sort_order
            self.icon_url = ""

        def download(self, downloader: "Downloader", path: Path) -> None:
            self.url = downloader.download_resource(self.url, path / self.name)

            self.icon_url = downloader.copy_resource_icon(self.file_type)

        def to_html(self) -> str:
            return '<a href="{}">{}{}</a>'.format(
                self.url, icon_tag(self.icon_url), self.name
            )

    def __init__(
        self,
        *args,
        root: "FolderModule.Folder",
        create_subfolder: bool = True,
        **kwargs
    ) -> None:
        super().__init__(*args, **kwargs)

        self.root = root
        self.create_subfolder = create_subfolder

    @classmethod
    def get_kwargs_from_dict(cls, dict_: dict) -> dict:
        kwargs = super().get_kwargs_from_dict(dict_)

        root = FolderModule.Folder()

        contents = dict_.get("contents", [])
        contents = sorted(
            contents, key=lambda file_: file_.get("sortorder", 0), reverse=True
        )

        for file_ in contents:
            current = root
            parts = Path(file_["filepath"]).parts[1:]

            while parts:
                folder_name = parts[0]

                if folder_name not in current.folders:
                    current.folders[folder_name] = FolderModule.Folder()

                current = current.folders[folder_name]
                parts = parts[1:]

            current.files.append(
                FolderModule.File(
                    file_["filename"],
                    file_["fileurl"],
                    file_.get("mimetype"),
                    file_.get("sortorder", 0),
                )
            )
        kwargs["root"] = root
        return kwargs

    def download_files(self, downloader: "Downloader") -> None:
        super().download_files(downloader)

        path = Path()

        if self.create_subfolder:
            path = Path(clean_file_name(self.name))

        self.root.download_files(downloader, path)


class ResourceModule(DownloadableModule):
    use_url = True

    @classmethod
    def get_files(
        cls, folder: "DownloadableModule.Folder",
    ) -> Iterator["DownloadableModule.File"]:
        for file_ in folder.files:
            yield file_

        for folder_ in folder.folders.values():
            yield from cls.get_files(folder_)

    def download_files(self, downloader: "Downloader") -> None:
        file_ = next(
            iter(
                sorted(
                    self.get_files(self.root),
                    key=lambda file_: file_.sort_order,
                    reverse=True,
                )
            ),
            None,
        )

        if not file_:
            logger.warning("resource-module %s is empty", self.id)
            super().download_files(downloader)
            return

        # Put the files in a subfolder if there are multiple files
        if len(self.root.files) == 1 or len(self.root.folders.values()) == 1:
            self.create_subfolder = False

        # Don't download the default icon
        self.icon_url = ""

        super().download_files(downloader)

        self.url = file_.url
        self.icon_url = downloader.copy_resource_icon(file_.file_type)


class FolderModule(DownloadableModule):
    @classmethod
    def get_kwargs_from_dict(cls, dict_: dict) -> dict:
        kwargs = super().get_kwargs_from_dict(dict_)

        kwargs["description"] = None
        return kwargs

    def download_files(self, downloader: "Downloader") -> None:
        super().download_files(downloader)
        self.description = self.root.to_html(self.icon_url)


class PageModule(DownloadableModule):
    use_url = True

    def download_files(self, downloader: "Downloader") -> None:
        super().download_files(downloader)
        self.url = path_to_url(
            Path(
                downloader.download_path
                / "resources"
                / clean_file_name(self.name)
                / "index.html"
            )
        )

    def get_url(self):
        return self.url


class MoodleAPIClient:
    def __init__(
        self, url: str, token: str = None, save_api_responses: bool = False
    ) -> None:
        self.url = url
        self.token = token
        self.save_api_responses = save_api_responses

        if save_api_responses:
            self.responses_path = Path("api_responses")
            self.responses_path.mkdir(exist_ok=True)

    def fetch_token(self, username: str, password: str) -> None:
        params = urllib.parse.urlencode(
            {
                "username": username,
                "password": password,
                "service": "moodle_mobile_app",
            }
        )

        url = "{}/login/token.php?{}".format(self.url, params)

        with urllib.request.urlopen(url) as file_:
            response = json.loads(file_.read().decode("utf-8"))
            self.token = response["token"]

    def _invoke_api(self, function: str, params: dict = None) -> Any:
        if params is None:
            params = {}

        params.update(
            {
                "wstoken": self.token,
                "wsfunction": function,
                "moodlewsrestformat": "json",
            }
        )

        url = "{}/webservice/rest/server.php?{}".format(
            self.url, urllib.parse.urlencode(params)
        )

        with urllib.request.urlopen(url) as file_:
            response = file_.read().decode("utf-8")

            if self.save_api_responses:
                with open(
                    self.responses_path
                    / clean_file_name(
                        function + "_" + str(uuid.uuid4()) + ".json"
                    ),
                    "w",
                    encoding="utf8",
                ) as file_:
                    file_.write(response)

            response = json.loads(response)
            return response

    def download_file(self, url: str, path: Path) -> None:
        logger.debug("downloading file %s", url)

        url += ("?" if url.find("?") == -1 else "&") + urllib.parse.urlencode(
            {"token": self.token}
        )

        with urllib.request.urlopen(url) as src, open(str(path), "wb") as dst:
            shutil.copyfileobj(src, dst)

    def core_webservice_get_site_info(self) -> Any:
        return self._invoke_api("core_webservice_get_site_info")

    def core_course_get_contents(self, course_id: int) -> Any:
        return self._invoke_api(
            "core_course_get_contents", {"courseid": course_id}
        )

    def core_enrol_get_users_courses(self, user_id: int) -> Any:
        return self._invoke_api(
            "core_enrol_get_users_courses", {"userid": user_id}
        )


def clean_file_name(file_name: str) -> str:
    if file_name == "..":
        raise ValueError('".." got used as a filename. This should not happen!')
    return re.sub('[<>:"/\\\\\\?\\*\x00-\x1f]', "", file_name).strip(" .")


def path_to_url(path: Path) -> str:
    return urllib.parse.quote(path.as_posix())


class IconRepository:
    def __init__(self) -> None:
        self.icons_archive = zipfile.ZipFile(
            pkg_resources.resource_filename("moodle_archiver", "icons.zip")
        )

        self.type_icons: Dict[str, str] = {}
        with self.icons_archive.open("types.json") as file_:
            self.type_icons = json.loads(file_.read().decode("utf-8"))

        self.module_icons: Set[str] = set()
        with self.icons_archive.open("modules.json") as file_:
            self.module_icons = set(json.loads(file_.read().decode("utf-8")))

    def __del__(self) -> None:
        self.icons_archive.close()

    def _copy_icon(self, icon_path: str, path: Path) -> None:
        logger.debug(
            'copying "%s" from icons.zip to "%s"', icon_path, str(path)
        )

        with self.icons_archive.open(icon_path) as src, open(path, "wb") as dst:
            shutil.copyfileobj(src, dst)

    def copy_resource_icon(self, type_: str, path: Path) -> Path:
        icon_name = "type/{}.png".format(self.type_icons.get(type_, "unknown"))
        path = path / icon_name

        if path.exists():
            return path

        self._copy_icon(icon_name, path)
        return path

    def copy_module_icon(
        self, name: str, path: Path, fallback_glob: str, fallback_url: str
    ) -> Path:
        icon_name = "mod/{}.svg".format(name)
        path = path / icon_name

        if path.exists():
            return path

        if name in self.module_icons:
            self._copy_icon(icon_name, path)
            return path

        other_path = next(glob.iglob(fallback_glob), None)
        if other_path:
            logger.debug("copying icon for module %s from other course", name)
            shutil.copyfile(other_path, path)
            return path

        logger.debug(
            "downloading icon for module %s from URL %s", name, fallback_url
        )

        with urllib.request.urlopen(fallback_url) as src, open(
            path, "wb"
        ) as dst:
            shutil.copyfileobj(src, dst)

        return path


class Downloader:
    def __init__(
        self,
        api_client: "MoodleAPIClient",
        icon_repository: "IconRepository",
        base_path: Path,
        download_path: Path,
        ignore_resources: Optional[List[str]] = None,
    ) -> None:

        self.api_client = api_client
        self.icon_repository = icon_repository
        self.base_path = base_path
        self.download_path = download_path
        self.ignore_resources = ignore_resources

        # Keep track of already downloaded files
        # Key: url, value: relative path on disk
        self.downloaded_files: Dict[str, str] = {}

        # Create directories
        (self.base_path / download_path).mkdir(parents=True, exist_ok=True)
        (self.base_path / download_path / "resources").mkdir(exist_ok=True)
        (self.base_path / download_path / "icons").mkdir(exist_ok=True)
        (self.base_path / download_path / "icons" / "mod").mkdir(exist_ok=True)
        (self.base_path / download_path / "icons" / "type").mkdir(exist_ok=True)

        # Load files.json
        self.downloaded_files_file = (
            self.base_path / download_path / "files.json"
        )

        if self.downloaded_files_file.exists():
            logger.debug(
                'loading downloaded files from "%s"',
                str(self.downloaded_files_file),
            )

            with self.downloaded_files_file.open(encoding="utf8") as file_:
                try:
                    self.downloaded_files = json.loads(file_.read())
                except json.JSONDecodeError:
                    logger.error(
                        'failed to parse "%s"', str(self.downloaded_files_file)
                    )

        self.used_files: Set[Path] = set()
        self.used_icons: Set[Path] = set()

    def save_downloaded_files(self) -> None:
        logger.debug(
            'saving downloaded files to "%s"', str(self.downloaded_files_file)
        )

        with self.downloaded_files_file.open("w", encoding="utf8") as file_:
            file_.write(json.dumps(self.downloaded_files, indent=4))

    @staticmethod
    def _delete_empty_folders(path: Path) -> None:
        while not next(path.iterdir(), None):
            path.rmdir()
            path = path.parent

    def delete_old_resources(self) -> None:
        old_resources = {
            self.base_path / urllib.parse.unquote(path)
            for path in self.downloaded_files.values()
        } - self.used_files

        downloaded_files_inverted = {
            self.base_path / urllib.parse.unquote(relative_url): url
            for url, relative_url in self.downloaded_files.items()
        }

        for path in old_resources:
            del self.downloaded_files[downloaded_files_inverted[path]]

            if path in self.used_icons:
                # Icons are not tracked in files.json but
                # sholud not be deleted.
                continue

            if not path.exists():
                logger.debug(
                    'removed unused and deleted file "%s" from files.json',
                    str(path),
                )
            else:
                logger.info('deleting unused tracked file "%s"', str(path))
                path.unlink()
                self._delete_empty_folders(path.parent)

    def delete_untracked_resources(self) -> None:
        files_to_keep = {
            Path("files.json"),
            Path("resources"),
            Path("icons"),
            Path("icons/mod"),
            Path("icons/type"),
        }

        for relative_url in self.downloaded_files.values():
            files_to_keep.add(
                Path(urllib.parse.unquote(relative_url)).relative_to(
                    self.download_path
                )
            )

        for path in self.used_icons:
            files_to_keep.add(path.relative_to(self.download_path))

        # This inner function recursively walks through the whole directory
        # tree and deletes files which are not present in files.json.
        def delete_files(path: Path) -> None:
            absolute_path = self.base_path / self.download_path / path

            if not absolute_path.exists():
                return

            if not (absolute_path.is_dir() or absolute_path.is_file()):
                return

            if absolute_path.is_dir():
                for child in absolute_path.iterdir():
                    delete_files(
                        child.relative_to(self.base_path / self.download_path)
                    )

                if not next(absolute_path.iterdir(), None):
                    if path not in files_to_keep:
                        logger.info(
                            'deleting untracked folder "%s"', str(absolute_path)
                        )
                        absolute_path.rmdir()

                return

            if path not in files_to_keep:
                logger.info('deleting untracked file "%s"', str(absolute_path))
                absolute_path.unlink()

        delete_files(Path())

    def copy_resource_icon(self, type_: str) -> str:
        path = self.base_path / self.download_path / "icons"
        path = self.icon_repository.copy_resource_icon(type_, path)

        self.used_icons.add(path)
        return path_to_url(path.relative_to(self.base_path))

    def copy_module_icon(self, name: str, fallback_url: str) -> str:
        path = self.base_path / self.download_path / "icons"

        glob_string = "{}/*_files/icons/mod/{}.svg".format(
            glob.escape(str(self.base_path)), name
        )

        path = self.icon_repository.copy_module_icon(
            name, path, glob_string, fallback_url
        )

        self.used_icons.add(path)
        return path_to_url(path.relative_to(self.base_path))

    def download_resource(self, url: str, path: Path) -> str:
        path = self.base_path / self.download_path / "resources" / path

        if url in self.downloaded_files:
            relative_url = self.downloaded_files[url]
            src = self.base_path / urllib.parse.unquote(relative_url)

            if src.exists():
                if not path.exists():
                    # Move the file to the correct location. This does not
                    # always immediately lead to the correct folder layout,
                    # but it should work well enough in most cases.
                    logger.debug('moving "%s" to "%s"', str(src), str(path))
                    path.parent.mkdir(parents=True, exist_ok=True)
                    src.rename(path)

                    # Delete empty parent folders
                    self._delete_empty_folders(src.parent)

                    relative_url = path_to_url(path.relative_to(self.base_path))
                    self.downloaded_files[url] = relative_url

                    self.used_files.add(path)
                else:
                    self.used_files.add(src)

                return relative_url

        if self.ignore_resources:
            for regex in self.ignore_resources:
                if re.match(regex, url):
                    logger.debug(
                        "skipping %s because it's on the blacklist", url
                    )
                    return url

        if path.exists():
            file_name = path.name
            path = path.parent / str(uuid.uuid4())
            path.mkdir()
            path = path / clean_file_name(file_name)
        else:
            path.parent.mkdir(parents=True, exist_ok=True)

        self.api_client.download_file(url, path)

        relative_url = path_to_url(path.relative_to(self.base_path))
        self.downloaded_files[url] = relative_url

        self.used_files.add(path)
        return relative_url

    def download_linked_files(self, text: str) -> str:
        link_pattern = (
            re.escape(self.api_client.url)
            + r"(/webservice)?(/pluginfile\.php/[^\"'<> ?]+)"
        )

        # Download all files in a text-block and replace the links
        return re.sub(
            link_pattern,
            lambda match: self.download_resource(
                # Download the file from the "/webservice"-URL
                self.api_client.url + "/webservice" + match.group(2),
                Path(
                    clean_file_name(
                        Path(urllib.parse.unquote(match.group(0))).name
                    )
                ),
            ),
            text,
        )


class MoodleArchiver:
    def __init__(
        self,
        api_client: "MoodleAPIClient",
        path: Path,
        ignore_resources: Optional[List[str]] = None,
        delete_old_resources: Optional[bool] = False,
        delete_untracked_resources: Optional[bool] = False,
    ) -> None:
        self.api_client = api_client
        self.path = path
        self.ignore_resources = ignore_resources
        self.delete_old_resources = delete_old_resources
        self.delete_untracked_resources = delete_untracked_resources
        self.icon_repository = IconRepository()

    def download_courses(
        self, course_filter: Optional[List[int]] = None
    ) -> None:
        try:
            info = self.api_client.core_webservice_get_site_info()
        except (ValueError, urllib.error.URLError, urllib.error.HTTPError):
            logger.error("the site couldn't be reached")
            return

        # Check if the token is valid
        if "exception" in info:
            logger.error(
                "token did not get accepted, reason: %s",
                info.get("message", "unknown"),
            )
            return

        # Update base-URL
        self.api_client.url = info["siteurl"]

        user_id = info["userid"]
        user_full_name = info["fullname"]

        logger.info("downloading courses for %s", user_full_name)

        courses: List[dict] = self.api_client.core_enrol_get_users_courses(
            user_id
        )

        # Filter out courses
        if course_filter:
            courses = [_ for _ in courses if _["id"] in course_filter]

        total_courses = len(courses)

        for (index, course) in enumerate(courses):
            id_ = course["id"]
            name = course["fullname"].strip()

            logger.info(
                "downloading course %s/%s: %s", index + 1, total_courses, name
            )

            self.download_course(name, id_)

    def download_course(self, name: str, id_: int) -> None:
        course = Course.from_dict(
            id_, name, self.api_client.core_course_get_contents(id_),
        )

        course_folder = Path(clean_file_name(name + "_files"))

        downloader = Downloader(
            self.api_client,
            self.icon_repository,
            self.path,
            course_folder,
            self.ignore_resources,
        )

        course.download_files(downloader)

        if self.delete_old_resources:
            downloader.delete_old_resources()

        if self.delete_untracked_resources:
            downloader.delete_untracked_resources()

        downloader.save_downloaded_files()

        html_file_path = self.path / clean_file_name(name + ".html")
        with open(html_file_path, "w", encoding="utf8") as file_:
            file_.write(course.to_html(self.api_client.url))


def main() -> None:
    # Parse arguments
    parser = argparse.ArgumentParser(
        description="download courses from Moodle using its REST API"
    )
    parser.add_argument("url", help="URL of the Moodle instance")
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument("--token", help="token for the Moodle API")
    group.add_argument(
        "--credentials",
        nargs=2,
        metavar=("USERNAME", "PASSWORD"),
        help="fetch token using credentials (alternative to --token)",
    )
    parser.add_argument(
        "--output-directory",
        help="where to save the courses",
        default="courses",
        dest="path",
        metavar="DIRECTORY",
    )
    parser.add_argument(
        "--courses",
        type=int,
        nargs="+",
        metavar="ID",
        help="specify which courses to download",
    )
    parser.add_argument(
        "--ignore-resources",
        type=str,
        nargs="+",
        metavar="REGEX",
        help="don't download resources which match these regular expressions",
    )
    parser.add_argument(
        "--delete-old-resources",
        help="delete the resources which are no longer linked in the course",
        action="store_true",
    )
    parser.add_argument(
        "--delete-untracked-resources",
        help="delete all untracked files in course folders",
        action="store_true",
    )
    group = parser.add_mutually_exclusive_group()
    group.add_argument(
        "--silent", help="don't show any messages", action="store_true"
    )
    group.add_argument(
        "--verbose", help="show all the messages", action="store_true"
    )
    parser.add_argument(
        "--save-api-responses",
        help="save the responses from the API for debugging purposes",
        action="store_true",
    )

    args = parser.parse_args()
    args.path = Path(args.path)

    # Set log level
    log_level = logging.INFO

    if args.silent:
        log_level = logging.CRITICAL
    if args.verbose:
        log_level = logging.DEBUG

    logging.basicConfig(level=log_level)

    # Remove trailing slash
    if args.url.endswith("/"):
        args.url = args.url[0:-1]

    # Add protocol
    if not args.url.startswith("http"):
        args.url = "https://" + args.url

    client = MoodleAPIClient(args.url, args.token, args.save_api_responses)

    if args.credentials:
        username, password = args.credentials

        try:
            client.fetch_token(username, password)
        except (
            KeyError,
            ValueError,
            urllib.error.URLError,
            urllib.error.HTTPError,
        ):
            logger.error("failed to log in")
            exit(1)

    archiver = MoodleArchiver(
        client,
        args.path,
        args.ignore_resources,
        args.delete_old_resources,
        args.delete_untracked_resources,
    )
    archiver.download_courses(args.courses)


if __name__ == "__main__":
    main()

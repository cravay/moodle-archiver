#!/usr/bin/env python3

import setuptools

with open("README.md", "r") as file_:
    long_description = file_.read()

setuptools.setup(
    name="moodle_archiver",
    version="0.0.1",
    author="Michael Schertenleib",
    author_email="michaelschertenleib@gmail.com",
    description="This tool can download courses from Moodle using its REST API.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/cravay/moodle-archiver",
    packages=setuptools.find_packages(),
    package_data={"moodle_archiver": ["icons.zip"]},
    entry_points={"console_scripts": ["moodle-archiver=moodle_archiver:main"]},
    classifiers=[
        "Programming Language :: Python :: 3",
        "Development Status :: 3 - Alpha",
        "License :: OSI Approved :: GNU Affero General Public License v3 or later (AGPLv3+)",
        "Operating System :: OS Independent",
        "Topic :: System :: Archiving",
    ],
    python_requires=">=3.6",
)

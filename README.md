# moodle-archiver
This tool can download courses from Moodle using its REST API. It creates an
HTML-file for each course and downloads the files linked in the course. Only
new files get downloaded.

## Installation
**Note:** You need python 3.6 or higher to run moodle-archiver.
```
pip install git+https://gitlab.com/cravay/moodle-archiver.git
```

## Usage
### Example usage
```
$ moodle-archiver https://school.moodledemo.net --credentials student moodle
INFO:moodle_archiver:downloading courses for Barbara Gardner
INFO:moodle_archiver:downloading course 1/13: Digital Literacy
INFO:moodle_archiver:downloading course 2/13: Psychology in Cinema
INFO:moodle_archiver:downloading course 3/13: The Impressionists
INFO:moodle_archiver:downloading course 4/13: Celebrating Cultures
INFO:moodle_archiver:downloading course 5/13: History: Russia in Revolution
...

$ tree courses/ -L 2
courses/
├── Digital Literacy.html
├── Digital Literacy_files
│   ├── files.json
│   ├── icons
│   └── resources
├── Psychology in Cinema.html
├── Psychology in Cinema_files
│   ├── files.json
│   ├── icons
│   └── resources
├── The Impressionists.html
├── The Impressionists_files
│   ├── files.json
│   ├── icons
│   └── resources
...
```

### Options
```
usage: moodle-archiver [-h] (--token TOKEN | --credentials USERNAME PASSWORD)
                       [--output-directory DIRECTORY] [--courses ID [ID ...]]
                       [--ignore-resources REGEX [REGEX ...]]
                       [--delete-old-resources] [--delete-untracked-resources]
                       [--silent | --verbose] [--save-api-responses]
                       url

download courses from Moodle using its REST API

positional arguments:
  url                   URL of the Moodle instance

optional arguments:
  -h, --help            show this help message and exit
  --token TOKEN         token for the Moodle API
  --credentials USERNAME PASSWORD
                        fetch token using credentials (alternative to --token)
  --output-directory DIRECTORY
                        where to save the courses
  --courses ID [ID ...]
                        specify which courses to download
  --ignore-resources REGEX [REGEX ...]
                        don't download resources which match these regular
                        expressions
  --delete-old-resources
                        delete the resources which are no longer linked in the
                        course
  --delete-untracked-resources
                        delete all untracked files in course folders
  --silent              don't show any messages
  --verbose             show all the messages
  --save-api-responses  save the responses from the API for debugging purposes
```
 
